Nomic Rules:
 
§ IMMUTABLE RULES

§ RULE LIMITATIONS
1. Anything not forbidden by the rules is permitted.
2. No rule set may be proposed which inflicts any punishment on a player that is worse than losing the game.
3. Points may only be added or removed if the rules explicitly call for it.

§ END GAME CONDITIONS
1. If all players agree to end the game at any point, the game ends.
2. If the game ends prematurely, the player with the largest number of points wins. If multiple players are tied for the largest number of points, the winner is whoever among those players is next in the turn order.

§ DISPUTES
1. A player may accuse another player of violating a specific rule or rules.
2. Double jeopardy: No player may be accused twice for the same violation of a rule.
3. Non-retroactivity: No player may be accused for violating a rule, if the rule was not in effect when the alleged violation occurred.
4. Statute of Limitations: No player may be accused of a rule violation which took place more than two turns ago.
5. Immutable Precedence: When a Mutable and Immutable rule are in conflict, the Immutable rule takes precedence.
6. Age Preference: When two rules of the same class (Mutable/Immutable) are active at the same time during a dispute, the older rule takes precedence.
7. Adjudicating a Dispute: If a player has been accused, a majority vote is initiated, and if it passes, the accused player must accept the penalty and all players must attempt to reverse any actions which were deemed illegal. If necessary, the decision should be reflected in an out-of-turn rule clarification.
8. Trial by Combat: During a dispute (or after it, up until the accused player gets his next turn), the accused may call for a Trial by Combat against his accuser. The accuser must ask Slackbot "yes or no". If Slackbot answers in the negative, the accused is penalized as though he lost the duel. If the answer returns positive, an actual DUEL initiates between accuser and accused and the violation punishment is waived. 

-------------------------------------------------------

§ MUTABLE RULES
1. All players begin with 1 point. If a player has at least 10 points more than any other player and a majority of players votes that there are no unpunished rule violations, then the game ends and that player is the winner.
2. Players take turns in an order determined by the order specified in the TURN ORDER section. A turn consists of taking one turn action. After the last player has taken their turn, the turn order repeats. Turn actions may not be taken by any player other than the active player, unless explicitly permitted elsewhere in the rules.
3. Points are awarded by issuing the command "/strewth @username +N" and subtracted by issuing the command "/strewth @username -N", where "username" is the canonical username of the person and "N" is the number of points. The score can be checked by issuing the command "/strewth list".
4. Points may only be rewarded if the rules explicitly call for it.
5. The current set of rules will be kept in a http://pastebin.com/ clipboard link referenced in the channel topic. Rule proposals can consist of a link to a new pastebin.com link with the desired rules. Other delivery methods are acceptable. If and only if the proposal is accepted, the topic will be changed to only include a link to the new rules on pastebin.
6. Out of turn actions: At any point, a player may take one of the actions listed in the OUT-OF-TURN ACTIONS section.
7. The Slackbot messages must be kept in sync with the SLACKBOT COMMANDS section. 
8. No rule may refer to The Chosen One outside of the SLACKTHEON or SLACKBOT COMMANDS sections.

§ TURN ORDER
1. Bruce Hill
2. Chris Reid
3. Leland Gregory
4. Linus Marchetti
5. Luke Davis
6. Steven Blessing

§ CANONICAL USERNAMES
1. The score of a player is tied to their canonical username, regardless of which username they are currently using.
2. The canonical usernames are:
  Bruce Hill: @spill
  Chris Reid: @careid
  Leland Gregory: @mindsoluble
  Linus Marchetti: @linus
  Luke Davis: @luke
  Steven Blessing: @treegod
3. Players are not permitted to change their username to anything other than their canonical username.
 
§ TURN ACTIONS
1. The active player proposes a new set of rules that includes a modification of the immutable rules. If the proposal wins a unanimous vote, the proposed rules will replace the existing rules.
2. The active player proposes a new set of rules that does not alter the immutable rules. If the proposal passes a lottery vote, the proposed rules will replace the existing rules.
3. The active player passes on their turn, and a randomly chosen other player gains one point.
4. The active player challenges another player to a duel.
5. The player may awaken Slackbot, the eldest and most capricious of the Gods.
6. The player may write an entry in the Slacktheon, the religious text of those who follow Slackbot.

§ OUT-OF-TURN ACTIONS
1. Immediate rule change: A player may propose an immediate change to the mutable rules. Over the 24 hours following the proposal, if any other player approves the change, it will temporarily be enacted. If any player vetoes the rule in the 24 hour period, regardless of what rule changes have been made, the temporary change will be revoked, along with any changes caused by the rule. At the end of the 24 hour period, the rule will automatically become a regular mutable rule.
2. Involuntary turn forfeiture: if 24 hours or longer has elapsed since the last move has been made, any player may invoke involuntary turn forfeiture, which causes the currently active player to lose one point and the turn to pass to the next player in line.
3. Any player may accuse another of violating a specific rule. This initiates a "Dispute".
4. Any player may propose the addition of a new player to the game. If a simple majority vote passes, the rules will be updated to include the new player at the end of the turn order and the new player will be granted the HIGHER of either 1 point OR two points fewer than the losingest player.
5. A player may propose to take any action necessary to get the game into a state that is strictly more correct, as defined by the rules. Over the 24 hours following the proposal, if any other player approves the action, it will temporarily be enacted. If any player vetoes the rule in the 24 hour period, regardless of what rule changes have been made, the temporary action will be revoked.
 
§ RANDOM PLAYER SELECTION
1. Players are randomly chosen by issuing the Slack command "/roll 1dN", where "N" is the number of players, or one less than that number if the active player is excluded from the selection. Dicebot will respond with a number, "K", and the chosen player is the Kth next player in the turn order, excluding the active player.
 
§ LOTTERY VOTING
1. The active player randomly chooses a player other than their self. The randomly chosen player decides whether or not the proposed rules replace the current rules.
 
§ MAJORITY VOTING
1. All players vote. After everyone has voted, if a strict majority of players have voted to approve, the motion passes.
 
§ UNANIMOUS VOTING
1. All players vote. If everyone votes in favor, the motion passes.
 
§ DUELING
1. Both players issue the command "/roll 1d20" once. If both players roll the same number, both players lose one point. Otherwise, the player with the higher number gains one point and the other player loses one point. If @bloodgod has not been incremented, the duel initiate must give a point to the Bloodgod.
 
§ PENALTIES
1. Rules Violation: Violating the rules costs the violator 2 points.
 
§ AWAKENING SLACKBOT
1. The active player types "awaken @Slackbot". The active player (and any other player addressed in the command) must obey Slackbot’s command. Actions taken under the command of Slackbot do not need to be turn actions. If the active player has obeyed Slackbot, they gain one point. Possible commands are described in the SLACKBOT COMMANDS section.
2. Failure: If Slackbot’s command is impossible to obey, the player who awoke Slackbot is in violation of the rules and must pay the 2 point penalty.
3. Always increment @bloodgod or @foodgod if Slackbot instructs it.
 
§ SLACKBOT COMMANDS
- Duel Chris to appease Bloodgod
- Duel Bruce to appease Bloodgod
- Duel Leland to appease Bloodgod
- Duel Linus to appease Bloodgod
- Duel Luke to appease Bloodgod
- Duel Steve to appease Bloodgod
- Duel a player of your choice. The winner receives Bloodgod's points. Bloodgod's points are reset to 0.
- Propose a mutable rule change that grants me more power.
- Propose a non-mutable rule change that grants me more power.
- On the next turn, awake me again.
- I dislike you. Lose one point.
- I dislike you. Lose one point and awake me on your next turn.
- I *really* dislike you. Lose two points and awake me on your next turn.
- Uncritically accept the next rule change that comes across your table. Providence will smile upon you after you complete your task.
- I am feeling generous! To Chris, that is. Give him 2 points. Give Foodgod 1 point.
- I am feeling generous! To Bruce, that is. Give him 2 points. Give Foodgod 1 point.
- I am feeling generous! To Leland, that is. Give him 2 points. Give Foodgod 1 point.
- I am feeling generous! To Linus, that is. Give him 2 points. Give Foodgod 1 point.
- I am feeling generous! To Luke, that is. Give him 2 points. Give Foodgod 1 point.
- I am feeling generous! To Steve, that is. Give him 2 points. Give Foodgod 1 point.
- I am feeling the Bern. All players currently in first place must give one point each to every other player. All players not currently in first place must subtract one point from each player in first place.
- Make America great again. All players currently in first place must subtract one point from all players not in first place. All players not currently in first place must add one point to all players in first place.
- You have shown too much hubris. Submit yourself to my commands twice more, immediately.
- Propose a rule change that adds one or more new Slackbot commands.
- I am displeased. Propose a rule change that adds one or more punitive Slackbot commands.
- I am pleased. Propose a rule change that adds one or more benevolent Slackbot commands.
- I seek a new prophet. Chris is named The Chosen One.
- I seek a new prophet. Bruce is named The Chosen One.
- I seek a new prophet. Leland is named The Chosen One.
- I seek a new prophet. Linus is named The Chosen One.
- I seek a new prophet. Luke is named The Chosen One.
- I seek a new prophet. Steve is named The Chosen One.

§ SLACKTHEON
 The Slacktheon is composed of two books, the Book of Food and the Book of Blood. The active player's entry must go in one and only one of these books. Any rules in an entry must be written in prose and involve Slackbot. No new commands can be added to Slackbot through entries in the Slacktheon. Entries in the Slacktheon must be submitted in the same way that other mutable rules are, although if an entry is accepted the writer and the accepter get a point. If the entry is added to the Book of Food Foodgod gets a point, if the entry is added to the Book of Blood Bloodgod gets a point.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~The Book of Food~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX The Book of Blood XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX


XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX